import React, { useState } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from '../firebase-config';

export const Login = () => {

  const [err, setErr] = useState(false)
  const [message, setMessage] = useState("");
  const navigate = useNavigate()

  const handleSubmit = async (e) => {
    // used for not refreshing the page when we press the Sign up button
    e.preventDefault()
    const email = e.target[0].value;
    const password = e.target[1].value;

    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        console.log(userCredential.user);
        navigate("/");
      })
      .catch((error) => {
        switch(error.code) {
          case 'auth/invalid-login-credentials':
            setErr(true)
            setMessage('Invalid login credentials !')
            break;
          case 'auth/missing-password':
            setErr(true)
            setMessage('Enter your password !')
            break;
          case 'auth/invalid-email':
            setErr(true)
            setMessage('Enter a valid email address !')
            break;
          default:
            setErr(true)
            setMessage(error.message)
            break;
        }
      });
  };

  return (
    <div className='formContainer'>
      <div className='formWrapper'>
        <span className='logo'>FriendsChat</span>
        <span className='title'>Login</span>
        <form onSubmit={handleSubmit}>
          <input type="email" placeholder='email' />
          <input type="password" placeholder='password' />
          <button>Log in</button>
          {err && <span className='errorMessage'>{message}</span>}
        </form>
        <p>Don't have an account? <Link to="/register">Sign up here.</Link></p>
      </div>
    </div>
  )
}

export default Login