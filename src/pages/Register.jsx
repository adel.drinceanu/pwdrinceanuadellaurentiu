import React, { useState } from 'react'
import Add from '../img/addAvatar.png'
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { auth, storage, db } from '../firebase-config'
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { doc, setDoc } from "firebase/firestore";
import { Link, useNavigate } from 'react-router-dom';

export const Register = () => {
    const [err, setErr] = useState(false);
    const [message, setMessage] = useState("");
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        // used for not refreshing the page when we press the Sign up button
        e.preventDefault()
        const displayName = e.target[0].value;
        const email = e.target[1].value;
        const password = e.target[2].value;
        const file = e.target[3].files[0];
        //console.log(file);

        try {
            const res = await createUserWithEmailAndPassword(auth, email, password);
            
             //Create a unique image name using displayName of the user and date
            const date = new Date().getTime();
            const storageRef = ref(storage, `${displayName + date}`);

            await uploadBytesResumable(storageRef, file).then(() => {
                getDownloadURL(storageRef).then(async (downloadURL) => {
                  try {
                    // update profile saving the displayName and uploaded photo
                    await updateProfile(res.user, {
                      displayName,
                      photoURL: downloadURL,
                    });
                    // create user on firestore
                    await setDoc(doc(db, "users", res.user.uid), {
                      uid: res.user.uid,
                      displayName,
                      email,
                      photoURL: downloadURL,
                    });
        
                    // create empty user chats on firestore to create relation between two users that have a chat
                    await setDoc(doc(db, "userChats", res.user.uid), {});

                    // after sucessfull register go to home page
                    navigate("/");
                  } catch (error) {
                    // console.log(err);
                  }
                });
              });
            } catch (error) {
              switch(error.code) {
                case 'auth/weak-password':
                  setErr(true)
                  setMessage('Password must contain at least 6 characters !')
                  break;
                case 'auth/email-already-in-use':
                  setErr(true)
                  setMessage('This email is already used !')
                  break; 
                default:
                  setErr(true)
                  setMessage(error.message)
                  break;
              }
            }
    };

    return (
        <div className='formContainer'>
            <div className='formWrapper'>
                <span className='logo'>FriendsChat</span>
                <span className='title'>Register</span>
                <form onSubmit={handleSubmit}>
                    <input type="text" placeholder='displaly name' />
                    <input type="email" placeholder='email' />
                    <input type="password" placeholder='password' />
                    <input type="file" id="file" />
                    <label htmlFor="file">
                        <img src={Add} alt="" />
                        <span>Add a profile image</span>
                    </label>
                    <button>Sign up</button>
                    {err && <span className='errorMessage'>{message}</span>}
                </form>
                <p>Already have an account? <Link to="/login">Log in here</Link>.</p>
            </div>
        </div>
    )
}

export default Register